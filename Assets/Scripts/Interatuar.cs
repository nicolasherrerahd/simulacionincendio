using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interatuar : MonoBehaviour
{
    public GameObject luzObjeto;


    public bool luz;
    public bool luzOnOff;
    
    public void OnOffLuz()
    {
        luzOnOff = !luzOnOff;

        if(luzOnOff == true)
        {
            luzObjeto.SetActive(true);
        }

        if(luzObjeto == false)
        {
            luzObjeto.SetActive(false);
        }
    }
}
