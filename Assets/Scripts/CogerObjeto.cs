using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CogerObjeto : MonoBehaviour
{
    public GameObject handPoint;
    private GameObject picketObject = null;


    void Update()
    {
        if(picketObject!=null)
        {
            if(Input.GetKey("r"))
            {
                picketObject.GetComponent<Rigidbody>().useGravity = true;

                picketObject.GetComponent<Rigidbody>().isKinematic = false;

                picketObject.gameObject.transform.SetParent(null);

                picketObject =  null;

            }
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if(other.gameObject.CompareTag("Objeto"))
        {
            if(Input.GetKey("e") && picketObject==null)
            {
                other.GetComponent<Rigidbody>().useGravity = false;

                other.GetComponent<Rigidbody>().isKinematic = true;

                other.transform.position = handPoint.transform.position;

                other.gameObject.transform.SetParent(handPoint.gameObject.transform);

                picketObject = other.gameObject;
            }
        }

    }

}
